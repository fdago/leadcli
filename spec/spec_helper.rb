require 'leadcli'
require 'fakeweb'

HOST = 'localhost'
TOKEN = "dea23643d95b70b6ac27a8003f9575659dc975737c266c56bcb20deed98aab3e3e8cd659752f180cf41a933473da89f92d8a5053c140ec07fe3b0deabee1c15b"

def with_configuration(options = {})
  Leadcli.configuration = nil
  Leadcli.configure do |configuration|
    options.each do |option, value|
      configuration.send("#{option}=", value)
    end
  end
  yield
end
