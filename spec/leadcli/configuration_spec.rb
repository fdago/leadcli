require 'spec_helper'

describe Leadcli::Configuration do
  let(:configuration) { Leadcli::Configuration.new }

  it "sets the host by default" do
    expect(configuration.host).to eq "new.eureo-tools.com"
  end

  it "allows the host to be overwritten" do
    expect { configuration.host = 'test.host' }.to change(configuration, :host).to('test.host')
  end

  it "sets the port" do
    expect { configuration.port = 1234 }.to change(configuration, :port).to(1234)
  end

  it "sets the token" do
    expect { configuration.token = '123456789' }.to change(configuration, :token).to("123456789")
  end

end

