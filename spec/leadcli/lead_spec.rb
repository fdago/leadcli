require 'spec_helper'
require 'json'

describe Leadcli::Lead do
  let(:params) { { subject: "Contact" }}
  let(:lead) { Leadcli::Lead.new }

  describe "#create" do
    it "creates it" do
      with_configuration(:host => HOST, :port => 3000, :token => TOKEN) do
        lead.create(params)
        expect(lead.last_response).to match(/Contact/)
      end
    end
    after(:each) do
      lead_id = JSON.parse(lead.last_response)['id']
      lead.delete(lead_id)
    end
  end

  describe "#delete" do
    context "when remote lead exists" do
      it "delete it" do
        with_configuration(:host => HOST, :port => 3000, :token => TOKEN) do
          lead.create(params)
          created_lead_id = JSON.parse(lead.last_response)['id']
          lead.delete(created_lead_id)

          deleted_lead_id = JSON.parse(lead.last_response)['id']
          expect(deleted_lead_id).to eq created_lead_id
        end
      end
    end
  end

end

