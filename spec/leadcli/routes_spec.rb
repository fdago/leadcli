require 'spec_helper'

class TestRoute
  include Leadcli::Routes
end

describe Leadcli::Routes do
  let!(:test_route) { TestRoute.new }
  let(:config) { { :host => 'test.host', :port => 1234 } }

  describe "create_endpoint" do
    it "returns :post and the create url" do
      options = { :foo => :bar }
      expect(test_route).to receive(:create_url).with(options).and_return('my_create_url')
      expect(test_route.create_endpoint(options)).to eq([:post, 'my_create_url'])
    end
  end

  describe "create_url" do
    it "constructs the url from the configuration" do
      with_configuration(config) do
        expect(test_route.create_url).to eq "http://test.host:1234/api/v1/leads"
      end
    end
    it "adds query parameters on to the url" do
      with_configuration(config) do
        url = test_route.create_url(:query => { :updated_at => '2013-07-19', :foo => :bar })
        expect(url).to match(/\?.*updated_at=2013-07-19/)
        expect(url).to match(/\?.*foo=bar/)
      end
    end
  end

end

