require 'spec_helper'

class ApiCallerTest
  include Leadcli::ApiCall
end

describe Leadcli::ApiCall do

  it "creates an ApiCaller object and tells it to make the call" do
    api_call_test = ApiCallerTest.new
    api_call = double('api_call')
    expect(api_call).to receive(:call).with(api_call_test)
    expect(Leadcli::ApiCaller).to receive(:new).with(:endpoint, { :foo => :bar }).and_return(api_call)
    api_call_test.api_call(:endpoint, { :foo => :bar })
  end

end
