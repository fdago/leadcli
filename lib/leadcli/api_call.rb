module Leadcli
  module ApiCall
    def api_call(endpoint, options={})
      api_caller = Leadcli::ApiCaller.new(endpoint, options)
      api_caller.call(self)
    end
  end
end
