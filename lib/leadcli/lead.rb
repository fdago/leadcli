require "leadcli/api_call"

module Leadcli
  class Lead
    include Leadcli::ApiCall

    attr_accessor :responses
    
    def initialize
      @responses = []
    end

    def create(params)
      api_call :create, :url_options => {}, :payload => { :lead => params }, :failure => :handle_failure, :success => :handle_success
    end

    def delete(lead_id)
      api_call :delete, :url_options => { :id => lead_id }, :failure => :handle_failure, :success => :handle_success
    end

    def handle_failure(response)
      Leadcli.debug("LEAD API CALL FAILED : #{response}")
      @responses << response
    end

    def handle_success(response)
      @responses << response
    end

    def last_response
      @responses.last
    end

  end
end

