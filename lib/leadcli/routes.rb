module Leadcli
  module Routes

    def create_endpoint(options)
      [:post, create_url(options)]
    end

    def create_url(options={})
      url = http_scheme.build(base_options.merge(:path => create_path))
      url.query = options[:query].map { |k,v| "#{k}=#{v}" }.join('&') if options[:query]
      url.to_s
    end

    def delete_endpoint(options)
      [:delete, delete_url(options)]
    end

    def delete_url(options)
      url = http_scheme.build(base_options.merge(:path => delete_path(options[:id])))
      url.to_s
    end

    protected

    def http_scheme
      if Leadcli.configuration.secure
        URI::HTTPS
      else
        URI::HTTP
      end
    end

    def base_options
      options = {}
      options[:host] = Leadcli.configuration.host
      options[:port] = Leadcli.configuration.port.to_i if Leadcli.configuration.port
      options
    end

    def create_path
      path = "/api/v1/leads"
      path
    end

    def delete_path(lead_id)
      path = "/api/v1/leads/#{lead_id}"
      path
    end

  end
end

