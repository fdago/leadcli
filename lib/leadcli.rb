require "leadcli/version"
require "leadcli/configuration"
require "leadcli/routes"
require "leadcli/api_caller"
require "leadcli/api_call"
require "leadcli/lead"

module Leadcli
  LOG_PREFIX = "** [Leadcli] "

  class << self
    attr_accessor :configuration

    def log(message)
      if logger
        logger.info(LOG_PREFIX + message)
      else
        puts(LOG_PREFIX + message)
      end
    end

    def debug(message)
      if logger
        logger.debug(LOG_PREFIX + message)
      else
        puts(LOG_PREFIX + message)
      end
    end

    def logger
      self.configuration && self.configuration.logger
    end


    #   @example
    #    
    #   Leadcli.configure do |config|
    #     config.token = '1234567890abcdef'
    #     config.host = 'localhost'
    #     config.port = '3000'
    #     config.logger = Rails.logger
    #   end

    def configure
      self.configuration ||= Configuration.new
      yield(configuration) if block_given?
    end

    def sync
      return true
    end
  end

end

