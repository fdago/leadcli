# Leadcli

## Installation

Add this line to your application's Gemfile:

    gem 'leadcli'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install leadcli

## Usage

```ruby
class ContactController < ApplicationController

  def create
    @contact = Contact.new(params_contact)
    if @contact.save
      send_lead(params_contact)
      redirect_to thanks_contact_path
    else
      render :new
    end
  end

  protected

  def send_lead(params)
    info = {
      subject: "Contact",
      content: params[:content],
      name: params[:name],
      email: params[:email]
    }
    Leadcli::Lead.new.create(info)
  end
  
end
```

